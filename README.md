Aplicatia web cu baza de date integrata se gaseste la urmatoarea adresa:

https://gitlab.upt.ro/raul.fedor/aplicatie-web-cu-baza-de-date-integrata.git

Pasii pentru rularea aplicatiei sunt urmatorii:
1. Se porneste meniul -> XAMPP.
2. Se ruleaza simultan atat Apache cat si MySQL ca servicii instalate pe sistemul de operare cat si cele din XAMPP.
3. Intr-un browser se scrie localhost pentru pagina de start.


